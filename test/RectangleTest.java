import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class RectangleTest {

    @Test
    void Testcase1(){
        RectangleArea rectangleArea= new RectangleArea(3d,5d);
        Double ans= rectangleArea.Area();
        assertEquals(15,ans);
    }
    @Test
    void Testcase2(){
        RectangleArea rectangleArea= new RectangleArea(3.3,4.5);
        Double ans= rectangleArea.Area();
        assertEquals(14.85,ans);
    }
}
