class RectangleArea {
    private Double length;
    private  Double width;

    RectangleArea(Double length, Double width) {
        this.length = length;
        this.width = width;
    }
    Double Area(){

        return (length * width);
    }
}
